# pogo-assistant

## Installation

### Windows
Target node version 14.19.0 for easy compatibility with node-hotkeys/iohook

## CLI

```
$ pogo-assistant --help

  Usage
    $ pogo-assistant

  Options
    --name  Your name

  Examples
    $ pogo-assistant --name=Jane
    Hello, Jane
```
