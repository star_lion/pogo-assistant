const nutjs = require('@nut-tree/nut-js')
const {produce} = require('immer')
const R = require('ramda')

const createBuddySlice = (set, get) => ({
  buddy: {

    uiTargets: {
      relative: {
        fractionRegionCorners: {
          buddyFromHome: [[0.227,0.890], [0.283,0.918]],
          play: [[0.307,0.412], [0.695,0.452]],
        }
      },
    },

  }
})

module.exports = createBuddySlice
