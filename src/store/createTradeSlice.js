const nutjs = require('@nut-tree/nut-js')
const importJsx = require('import-jsx')
const {produce} = require('immer')
const R = require('ramda')
const {exec} = require('child_process')

const utils = require('nut-js-utils')
const {relativeFractionPositionToAbsolutePoint} = utils

const {mouse, getWindows, Point, sleep} = nutjs

const DEBUG = false

const createTradeSlice = (set, get) => ({
  trade: {
    count: 0,
    automaticMode: true,
    status: 'Ready',
    timeoutId: undefined,
    steps: [
      {
        number: 1,
        description: 'Open trade screen',
        animationTime: DEBUG ? 2 : 6,
        n9Position: {
          absolutePoint: new Point(2350, 1322),
          relativeFraction: [0.815, 0.735],
        },
        s7Position: {
          absolutePoint: new Point(2895, 1429),
          relativeFraction: [0.817, 0.841]
        }
      },
      {
        number: 2,
        description: 'Select mon',
        animationTime: DEBUG ? 2 : 2,
        n9Position: {
          absolutePoint: new Point(2056, 924),
          relativeFraction: [0.196, 0.320]
        },
        s7Position: {
          absolutePoint: new Point(2572, 976),
          relativeFraction: [0.187, 0.365]
        }
      },
      {
        number: 3,
        description: 'Confirm mon selection',
        animationTime: DEBUG ? 2 : 2,
        n9Position: {
          absolutePoint: new Point(2213, 1381),
          relativeFraction: [0.504, 0.818]
        },
        s7Position: {
          absolutePoint: new Point(2697, 1387),
          relativeFraction: [0.505, 0.808]
        }
      },
      {
        number: 4,
        description: 'Confirm trade',
        animationTime: DEBUG ? 2 : 24,
        n9Position: {
          absolutePoint: new Point(2039, 1126),
          relativeFraction: [0.150, 0.520]
        },
        s7Position: {
          absolutePoint: new Point(2547, 1122),
          relativeFraction: [0.138, 0.531]
        }
      },
      {
        number: 5,
        description: 'Close received mon',
        animationTime: DEBUG ? 2 : 2,
        n9Position: {
          absolutePoint: new Point(2191, 1483),
          relativeFraction: [0.504, 0.909]
        },
        s7Position: {
          absolutePoint: new Point(2718, 1491),
          relativeFraction: [0.500, 0.921]
        }
      },
    ],

    executeStep: async () => {
      const {log, logInPlace} = get().app
      const {steps} = get().trade
      const {n9, s7} = get().app.devices
      const {n9Position, s7Position} = steps[0]
      let x0, y0, x1, y1
      let n9Point, s7Point
      if (n9.region != undefined && s7.region !== undefined) {
        ;[x0, y0] = [
          n9.region.left + parseInt(n9.region.width * n9Position.relativeFraction[0]),
          n9.region.top + parseInt(n9.region.height * n9Position.relativeFraction[1])
        ]
        ;[x1, y1] = [
          s7.region.left + parseInt(s7.region.width * s7Position.relativeFraction[0]),
          s7.region.top + parseInt(s7.region.height * s7Position.relativeFraction[1])
        ]
        n9Point = relativeFractionPositionToAbsolutePoint(n9Position.relativeFraction, n9.region)
        s7Point = relativeFractionPositionToAbsolutePoint(s7Position.relativeFraction, s7.region)
      } else {
        ;[x0, y0] = n9position.absolutepoint
        ;[x1, y1] = s7position.absolutepoint
        n9Point = n9Position.absolutePoint
        s7Point = s7Position.absolutePoint
      }

      // logInPlace(`,${steps[0].number}`)
      await sleep(100)
      await mouse.setPosition(n9Point)
      await sleep(100)
      await mouse.leftClick()
      await sleep(100)
      await mouse.setPosition(s7Point)
      await sleep(100)
      await mouse.leftClick()
    },

    executeAutomaticStep: () => {
      const {log, logInPlace} = get().app
      log('executing automatic step')

      set(produce(state => {
        const {steps, nextStepEvent, executeStep, executeAutomaticStep, count} = state.trade
        const currentStep = steps[0]
        const {animationTime, number} = currentStep
        if (count === 0) {
          state.app.toggleAllDevicesPower()
          state.app.beep()
          return
        }
        executeStep()
        if (number === 5 && count > 0) state.trade.count -= 1
        state.trade.timeoutId = setTimeout(() => {
          nextStepEvent()
          executeAutomaticStep()
        }, animationTime * 1000)
      }
    ))},

    setTimeoutId: (timeoutId) => set(produce(state => {
      state.trade.timeoutId = timeoutId
    })),

    toggleAutomaticMode: () => set(produce(state => {
      // if mode changing to manual and next automatic step queued, cancel it
      const {automaticMode, timeoutId} = get().trade
      if (automaticMode === true && timeoutId != undefined) {
        clearTimeout(timeoutId)
        state.trade.timeoutId = undefined
      }
      state.trade.automaticMode = !state.trade.automaticMode
    })),

    nextStepEvent: () => set(produce(state => {
      utils.arrayCycleForwards(state.trade.steps)
    })),

    previousStepEvent: () => set(produce(state => {
      utils.arrayCycleBackwards(state.trade.steps)
    })),

    increaseTradeCount: () => set(produce(state => {
      if (state.trade.count < 100) state.trade.count++
    })),

    decreaseTradeCount: () => set(produce(state => {
      if (state.trade.count > 0) state.trade.count -= 1
    })),

    increaseAnimationTimeSeconds: (seconds) => set(produce(state => {
      state.trade.steps.forEach((step) => step.animationTime = step.animationTime + seconds)

    }))
  }
})

module.exports = createTradeSlice
