const create = require('zustand').default
const importJsx = require('import-jsx')
const immer = require('immer')
const nutjs = require('@nut-tree/nut-js')
const R = require('ramda')

const {mouse, getWindows, Point} = nutjs
const {produce} = immer

const utils = require('nut-js-utils')
const createAppSlice = require('./createAppSlice.js')
const createTradeSlice = require('./createTradeSlice.js')
const createGiftSlice = require('./createGiftSlice.js')
const createBattleSlice = require('./createBattleSlice.js')

const DEBUG = false

const useStore = create((set, get) => ({
  ...createAppSlice(set, get),
  ...createTradeSlice(set, get),
  ...createGiftSlice(set, get),
  ...createBattleSlice(set, get),
  ...{

    debug: {
      logs: [],

      mousePosition: [0, 0],

      autoClickIntervalId: undefined,

      trackMouse: () => {
        // setInterval(async () => {
        //   const mousePosition = await mouse.getPosition()
        //   set(produce(state => { state.debug.mousePosition = [mousePosition.x, mousePosition.y] }))
        // }, 500)
      },

      toggleAutoClick: () => set(produce(state => {
        if (state.debug.autoClickIntervalId == undefined) {
          state.debug.autoClickIntervalId = setInterval(() => {
            mouse.leftClick()
          }, 500)
        } else {
          clearInterval(state.debug.autoClickIntervalId)
          state.debug.autoClickIntervalId = undefined
        }
      })),

      log: (log) => set(produce(state => {
        state.logs.push(log)
      }))

    }
  }
}))


const store = useStore.getState()

store.app.getDevices()
store.debug.trackMouse()
store.trade.increaseAnimationTimeSeconds(2)

module.exports = useStore
