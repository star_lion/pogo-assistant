const nutjs = require('@nut-tree/nut-js')
const {produce} = require('immer')
const R = require('ramda')
const utils = require('nut-js-utils')

const {mouse, straightTo, sleep, getWindows, Point, randomPointIn, screen} = nutjs

const DEBUG = false
nutjs.screen.config.autoHighlight = true
mouse.config.mouseSpeed = 4000
mouse.config.autoDelayMs = 100

const createGiftSlice = (set, get) => ({
  gift: {
    receiveMode: true,
    count: 30,

    uiTargets: {
      relative: {
        fractionRegionCorners: {
          send: [[], []],
          gift: [[0.42, 0.62], [0.61, 0.7]],
          open: [[0.31, 0.80], [0.72, 0.84]],
          close: [[0.468, 0.898], [0.534, 0.927]],
          leftSwipeHandle: [[0.12, 0.835], [0.225, 0.914]],
          rightSwipeHandle: [[0.786, 0.834], [0.857, 0.905]],
        },
        positions: {
          sendGiftMessage: [0.883, 0.426],
          giftBackdrop: [0.086, 0.084],
        }
      },
    },

    toggleMode: () => set(produce(state => state.gift.receiveMode == !state.gift.receiveMode)),

    increaseGiftCount: () => set(produce(state => {
      if (state.gift.count < 30) state.gift.count++
    })),

    decreaseGiftCount: () => set(produce(state => {
      if (state.gift.count > 0) state.gift.count--
    })),

    receiveGift: async () => {
      const state = get()
      const {logInPlace} = state.app
      const n9Region = state.app.devices.n9.region

      const {fractionRegionCorners, positions} = state.gift.uiTargets.relative
      const giftBackdropPoint = utils.relativeFractionPositionToAbsolutePoint(positions.giftBackdrop, n9Region)
      const giftBackdropColor = (await screen.colorAt(giftBackdropPoint)).toHex()
      const validBackDropColors = ['#72d9f1ff', '#73d1f6ff', '#63d8feff'] // team colors instinct, valor, mystic
      const doesGiftExist = validBackDropColors.includes(giftBackdropColor)

      logInPlace(`exists: ${doesGiftExist},${giftBackdropColor},`)
      if (doesGiftExist == false) return doesGiftExist

      const regions = R.map((fractionRegionCorner) => utils.relativeFractionRegionCornersToAbsoluteRegion(fractionRegionCorner, n9Region))(fractionRegionCorners)
      const randomizedPointsPromises = R.map((region) => randomPointIn(region), regions)
      const randomizedPoints = R.zipObj(
        Object.keys(randomizedPointsPromises),
        await Promise.all(Object.values(randomizedPointsPromises))
      )

      // R.forEach(async (region) => {
      //   await screen.highlight(region)
      //   // await nutjs.sleep(2000)
      // })(Object.values(regions))

      const {gift, open, leftSwipeHandle, rightSwipeHandle, close} = randomizedPoints

      // [point, shouldClick, postDelayMs, log]
      const eventsPreCheck = [
        [gift, true, 4000],
        [open, true],
      ]
      await utils.executeEventSequence(eventsPreCheck, logInPlace)

      // check if bag full or daily limit reached
      logInPlace('checking bag/daily limits:')
      const color = (await screen.colorAt(utils.relativeFractionPositionToAbsolutePoint(positions.sendGiftMessage, n9Region))).toHex()
      const messageFailColors = ['#ffffffff', '#80e3eaff']
      const didGiftOpenFail = messageFailColors.includes(color)

      logInPlace(`${didGiftOpenFail} (${color}),`)

      if (didGiftOpenFail == false) {
        const eventsPostCheck = [
          [close, false, 1000, 'closing'],
          [, true, 200],
          [, true, 200],
          [, false, 2000],
        ]

        await utils.executeEventSequence(eventsPostCheck, logInPlace)
        logInPlace('swiping')
        await mouse.setPosition(rightSwipeHandle)
        await mouse.pressButton(0)
        await mouse.move(straightTo(leftSwipeHandle))
        await mouse.releaseButton(0)
        await sleep(5000)
      }
      return didGiftOpenFail

    },

    receiveGifts: async () => {
      const state = get()
      if (state.gift.count == 0) {
        state.app.log('Receiving complete')
        state.app.toggleN9Power()
        return
      }
      state.app.log('Receiving gift:')
      const didGiftOpenFail = await state.gift.receiveGift()

      if (didGiftOpenFail == false) {
        set(produce(state => {state.gift.count--}))
        state.gift.receiveGifts()
      } else {
        state.app.logInPlace('cancel receiving')
        state.app.toggleN9Power()
        set(produce(state => {state.gift.count = 0}))
      }
    },

    sendGift: () => set(produce(state => {
    })),

    sendGifts: () => set(produce(state => {
      if (state.gift.count == 0) return
      get().gift.sendGift()
    })),

  }
})

module.exports = createGiftSlice
