const nutjs = require('@nut-tree/nut-js')
const {randomPointIn, mouse} = nutjs
const {produce} = require('immer')
const R = require('ramda')
const {relativeFractionRegionCornersToAbsoluteRegion} = require('nut-js-utils')

const createBattleSlice = (set, get) => ({
  battle: {

    uiTargets: {
      relative: {
        fractionRegionCorners: {
          leftChargeMove: [[0.256,0.878], [0.331,0.914]],
          centerChargeMove: [[0.461,0.873], [0.534,0.912]],
          rightChargeMove: [[0.662,0.873], [0.737,0.912]],
          fastMove: [[0.442,0.792], [0.552,0.836]],
          topAlternate: [[0.848,0.592], [0.936,0.632]],
          bottomAlternate: [[0.843,0.679], [0.945,0.724]],
          shieldCancel: [[0.404,0.911], [0.596,0.930]],
        }
      },
    },

    tap: async (fractionRegionCorners, containerRegion) => {
      const region = relativeFractionRegionCornersToAbsoluteRegion(fractionRegionCorners, containerRegion)
      const randomizedPoint = await randomPointIn(region)
      mouse.setPosition(randomizedPoint)
      mouse.leftClick()
    },

  }
})

module.exports = createBattleSlice
