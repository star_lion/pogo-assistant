const nutjs = require('@nut-tree/nut-js')
const {produce} = require('immer')
const R = require('ramda')
const {exec} = require('child_process')

const {getWindows, screen} = nutjs

const createAppSlice = (set, get) => ({
  app: {

    tabs: {
      Trade: {
        name: 'Trade',
      },
      Gift: {
        name: 'Gift',
      },
      Buddy: {
        name: 'Buddy',
      },
      Battle: {
        name: 'Battle',
      },
      Raid: {
        name: 'Raid',
      },
      Friends: {
        name: 'Friends',
      },
      Debug: {
        name: 'Debug',
      },
    },

    defaultTab: 'Trade',

    activeTab: 'Trade',

    setActiveTab: (tab) => set(produce(state => {
      state.app.activeTab = tab
    })),

    devices: {
      n9: {
        title: 'SM-N960U1',
        serial: '299b2eedfb1c7ece',
        window: undefined,
        region: undefined
      },
      s7: {
        title: 'SM-G930T',
        serial: '3e6a404e',
        window: undefined,
        region: undefined
      }
    },

    logs: [],

    getDevices: async () => {
      const devices = get().app.devices
      const windows = await getWindows()
      const [windowTitles, windowRegions] = await Promise.all([
        Promise.all(windows.map((window) => window.title)),
        Promise.all(windows.map((window) => window.region))
      ])

      const n9Index = R.findIndex(R.equals(devices.n9.title), windowTitles)
      if (n9Index != -1) {
        screen.highlight(windowRegions[n9Index])
        set(produce(state => {
          state.app.devices.n9.window = windows[n9Index]
          state.app.devices.n9.region = windowRegions[n9Index]
        }))
      } else {
        set(produce(state => {
          state.app.devices.n9.window = undefined
          state.app.devices.n9.region = undefined
        }))
      }

      const s7Index = R.findIndex(R.equals(devices.s7.title), windowTitles)
      if (s7Index != -1) {
        screen.highlight(windowRegions[s7Index])
        set(produce(state => {
          state.app.devices.s7.window = windows[s7Index]
          state.app.devices.s7.region = windowRegions[s7Index]
        }))
      } else {
        set(produce(state => {
          state.app.devices.s7.window = undefined
          state.app.devices.s7.region = undefined
        }))
      }
    },

    pasteToN9: (text) => {
      const serial = get().app.devices.n9.serial
      exec(`adb -s ${serial} shell "input text ${text} && input keyevent 61 && input keyevent 66"`)
    },

    pasteToS7: (text) => {
      const serial = get().app.devices.s7.serial
      exec(`adb -s ${serial} shell "input text ${text} && input keyevent 61 && input keyevent 66"`)
    },

    pasteToAllDevices: (text) => {
      const {pasteToN9, pasteToS7} = get().app
      pasteToN9(text)
      pasteToS7(text)
    },

    toggleDevicePower: (serial) => {
      get().app.log(`Toggling power for device ${serial}`)
      exec(`adb -s ${serial} shell input keyevent 26`)
    },

    toggleN9Power: () => {
      const n9Serial = get().app.devices.n9.serial
      get().app.log(`Toggling power for n9`)
      exec(`adb -s ${n9Serial} shell input keyevent 26`)
    },

    toggleAllDevicesPower: () => {
      get().app.log(`Toggling power for all devices`)
      const n9Serial = get().app.devices.n9.serial
      const s7Serial = get().app.devices.s7.serial
      exec(`adb -s ${n9Serial} shell input keyevent 26`)
      exec(`adb -s ${s7Serial} shell input keyevent 26`)
    },

    log: (text) => set(produce(state => {
      const now = new Date()
      const timestamp = `[${now.toLocaleTimeString('en-US')}]`
      state.app.logs.push(`${timestamp} ${text}`)
    })),

    logInPlace: (text) => set(produce(state => {
      const logsLength = get().app.logs.length
      const latestLogIndex = logsLength - 1
      if (logsLength == 0) {
        state.app.log(text)
      } else {
        state.app.logs[latestLogIndex] = state.app.logs[latestLogIndex] + text
      }
    })),

    beep: (pitch=400, durationMs=100) => {
      if (process.platform != 'win32') return
      exec(`powershell.exe [console]::beep(${pitch},${durationMs})`)
    },

    beepLow: (durationMs=100) => {
      get().app.beep(200, durationMs)
    },

    beepHigh: (durationMs=100) => {
      get().app.beep(600, durationMs)
    }

  }
})

module.exports = createAppSlice
