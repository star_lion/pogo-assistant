'use strict';
const React = require('react')
const ink = require('ink')
const {Text, Box} = ink
const {Tabs, Tab} = require('ink-tab')
const hotkeys = require('node-hotkeys')
const importJsx = require('import-jsx')
const R = require('ramda')

const useStore = require('./store/useStore.js')
const LogWindow = importJsx('ink-components/LogWindow')({React, ink})

const tabComponents = {
  Trade: importJsx('./components/Trade.js'),
  Gift: importJsx('./components/Gift.js'),
  Buddy: importJsx('./components/Buddy.js'),
  Battle: importJsx('./components/Battle.js'),
  Raid: importJsx('./components/Raid.js'),
  Friends: importJsx('./components/Friends.js'),
  Debug: importJsx('./components/Debug.js'),
}

const toTitleCase = (string) => `${string[0].toUpperCase()}${string.slice(1).toLowerCase()}`

const Assistant = (cliFlags) => {
  const store = useStore(state => state)
  const {tabs, defaultTab, activeTab, setActiveTab, logs} = store.app
  const ActiveTabComponent = tabComponents[activeTab] || undefined
  const defaultValue = (cliFlags.tab !== undefined) ? toTitleCase(cliFlags.tab) : defaultTab

  return (
    <>
      <Box
        margin={1}
        borderColor='white'
        borderStyle='single'
        flexDirection='column'
      >
        <Box
          paddingX={2}
          justifyContent='space-between'
        >
          <Text>Pogo Assistant</Text>
          <Text>[Alt-Q]uit</Text>
        </Box>

        <Box
          borderColor='white'
          borderStyle='single'
        >
          <Tabs
            keyMap={{useTab: true, useNumber: false, previous: ['h'], next: ['l']}}
            showIndex={true}
            defaultValue={defaultValue}
            onChange={setActiveTab}
          >
            {R.map(({name}) => (
              <Tab key={name} name={name}>{name}</Tab>
            ), R.values(tabs))}
          </Tabs>

        </Box>

        {ActiveTabComponent != undefined
          ? <ActiveTabComponent/>
          : undefined
        }


      </Box>
      <Box
        paddingX={1}
        flexDirection='column'
      >
        <LogWindow logs={logs} />
      </Box>
    </>
  )
}

module.exports = Assistant

process.on('exit', () => {
  hotkeys.iohook.unload()
})

hotkeys.on({
  hotkeys: 'alt + q',
  matchAllModifiers: true,
  callback: () => {
    process.exit()
  }
})
