'use strict';
const React = require('react')
const {Text, Box} = require('ink')

const Raid = () => {
  return (
    <Box
      borderColor='white'
      borderStyle='single'
    >
      <Text>
        Raid Component
      </Text>
    </Box>
  )
}

module.exports = Raid
