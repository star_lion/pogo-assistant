'use strict';
const React = require('react')
const {Text, Box, useInput} = require('ink')
const hotkeys = require('node-hotkeys')

const useStore = require('../store/useStore')

const {exec} = require('child_process')

const toggleDevicePower = (serial) => {exec(`adb -s ${serial} shell input keyevent 26`)}

const Debug = () => {
  const debug = useStore(state => state.debug)
  const {mousePosition} = debug
  const {n9, s7} = useStore(state => state.app.devices)

  useInput((input, key) => {
    if (input == 'p') {
      toggleDevicePower(n9.serial)
      toggleDevicePower(s7.serial)
    }
  }, {isActive: true})

  return (
    <>
      <Box
        marginX={0}
        borderColor='white'
        borderStyle='single'
        flexDirection='column'
      >
        <Text>Mouse Position: {mousePosition[0]}, {mousePosition[1]}</Text>
      </Box>
      <Box
        marginX={2}
        justifyContent='space-around'
      >
        <Text>[T]est</Text>
        <Text>[Alt-A]utoclick</Text>
        <Text>[P]ower</Text>
      </Box>
    </>
  )
}

const {toggleAutoClick} = useStore.getState().debug

hotkeys.on({
  hotkeys: 'alt + a',
  matchAllModifiers: true,
  callback: () => {
    if (useStore.getState().app.activeTab == 'Debug') toggleAutoClick()
  }
})

module.exports = Debug
