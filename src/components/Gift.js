'use strict';
const React = require('react')
const {Text, Box, Newline, useInput} = require('ink')
const hotkeys = require('node-hotkeys')
const R = require('ramda')

const useStore = require('../store/useStore.js')

const Gift = () => {
  const app = useStore(state => state.app)
  const n9Region = app.devices.n9.region
  const giftState = useStore(state => state.gift)

  const {receiveMode, count} = giftState
  const {getDevices} = app

  const {
    increaseGiftCount,
    decreaseGiftCount,
    receiveGifts,
    sendGifts
  } = giftState

  useInput((input, key) => {
    if (key.upArrow) increaseGiftCount()
    if (key.downArrow) decreaseGiftCount()
    if (input == 'r') receiveGifts()
    if (input == 's') sendGifts()
    if (input == 'g') getDevices()
  }, {isActive: true})

  return (
    <>
      <Box
        borderColor='white'
        borderStyle='single'
        flexDirection='column'
      >
        <Text> Mode: {receiveMode == true ? 'Receive' : 'Send'} </Text>
        <Text> Count: {count} </Text>
        <Text> N9 Measured: {n9Region != undefined ? 'true' : 'false'} </Text>
        <Text> Exclusion String: !Level </Text>
      </Box>

      <Box
        paddingX={2}
        marginBottom={1}
        justifyContent='space-around'
      >
        <Text>Paste Strings:</Text>
        <Text>[Ctrl-1]*</Text>
        <Text>[Ctrl-2]!level</Text>
        <Text>[Ctrl-3]!level&interactable</Text>
      </Box>

      <Box
        paddingX={2}
        justifyContent='space-around'
      >
        <Text>[R]eceive</Text>
        <Text>[S]end</Text>
        <Text>[G]et Devices</Text>
        <Text>[P]aste String</Text>
        <Text>[F]riends List</Text>
        <Text>[↑/↓]Gift Count</Text>
      </Box>
    </>
  )
}

const {pasteToN9} = useStore.getState().app

hotkeys.on({
  hotkeys: R.range(0,10).map((number) => `ctrl + ${number}`).join(', '),
  matchAllModifiers: true,
  triggerAll: true,
  callback: (hotkey) => {
    if (useStore.getState().app.activeTab != 'Gift') return
    const number = parseInt(R.last(hotkey))
    if (number == 1) pasteToN9('\\*')
    if (number == 2) pasteToN9('!level')
    if (number == 3) pasteToN9('!level\\&interactable')
  }
})

module.exports = Gift
