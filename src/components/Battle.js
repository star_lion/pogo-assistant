'use strict';
const React = require('react')
const {Text, Box} = require('ink')
const hotkeys = require('node-hotkeys')

const useStore = require('../store/useStore');
const {getActiveWindow} = require('@nut-tree/nut-js');

const keybindings = `
   [w] - fast attack     [r] - top alternate
[a][s][d] - charge moves [f] - bottom alternate
   [x] - cancel shield  `

const Battle = () => {
  return (
    <Box
      borderColor='white'
      borderStyle='single'
    >
      <Text>{keybindings}</Text>
    </Box>
  )
}

module.exports = Battle

const store = useStore.getState()
const {log} = store.app
const {tap} = store.battle

let throttleTimer
const throttle = (callback, time) => {
  if (throttleTimer) return;
    throttleTimer = true;
    setTimeout(() => {
        callback();
        throttleTimer = false;
    }, time);
}

hotkeys.on({
  hotkeys: 'w,a,s,d,r,f,x',
  triggerAll: true,
  callback: async (key) => {
    const state = useStore.getState()
    const n9Title = state.app.devices.n9.title
    const isBattleActive = state.app.activeTab != 'Battle'
    const isDeviceWindowFocused = (await (await getActiveWindow()).title == n9Title)
    if (!isBattleActive || !isDeviceWindowFocused) return

    const n9Region = state.app.devices.n9.region
    const {fractionRegionCorners} = state.battle.uiTargets.relative
    const uiTargetLookup = {
      'w': fractionRegionCorners.fastMove,
      'a': fractionRegionCorners.leftChargeMove,
      's': fractionRegionCorners.centerChargeMove,
      'd': fractionRegionCorners.rightChargeMove,
      'r': fractionRegionCorners.topAlternate,
      'f': fractionRegionCorners.bottomAlternate,
      'x': fractionRegionCorners.shieldCancel,
    }
    throttle(() => (tap(uiTargetLookup[key], n9Region)), 100)
  }
})
