'use strict';
const React = require('react')
const {Text, Box, useInput} = require('ink')
const hotkeys = require('node-hotkeys')
const R = require('ramda')

const useStore = require('../store/useStore.js')

const Trade = () => {
  const store = useStore(state => state)
  const {trade} = store
  const {steps, automaticMode, status, count} = trade
  const {
    toggleAutomaticMode,
    nextStepEvent,
    previousStepEvent,
    increaseTradeCount,
    decreaseTradeCount,
    executeAutomaticStep,
    executeStep,
  } = trade

  const getDevices = useStore(state => state.app.getDevices)
  const {n9, s7} = useStore(state => state.app.devices)
  const connectedDevices = []
  if (n9.region != undefined) connectedDevices.push('n9')
  if (s7.region != undefined) connectedDevices.push('s7')

  const currentTradeStep = steps[0]
  const {description, animationTime, number} = currentTradeStep

  useInput((input, key) => {
    if (input === 'm') toggleAutomaticMode()
    if (input === 'g') getDevices()

    if (key.upArrow) increaseTradeCount()
    if (key.downArrow) decreaseTradeCount()

    if (key.rightArrow) nextStepEvent()
    if (key.leftArrow) previousStepEvent()

    if (input === 's') {
      if (automaticMode === true) {
        executeAutomaticStep()
      } else {
        executeStep()
        nextStepEvent()
      }
    }
  }, {isActive: true})

  return (
    <>

      <Box>

        <Box
          width={14}
          borderColor='white'
          borderStyle='single'
          alignItems='flex-end'
          flexDirection='column'
        >
          <Text>Mode</Text>
          <Text>Trade Count</Text>
          <Text>Step</Text>
          <Text>Devices</Text>
          <Text>Status</Text>
        </Box>

        <Box
          borderColor='white'
          borderStyle='single'
          flexDirection='column'
          flexGrow={1}
        >
          <Text> {automaticMode === true ? 'Automatic' : 'Manual' } </Text>
          <Text> {count} </Text>
          <Text> {number}) {description}, wait {animationTime}s </Text>
          <Text> {(connectedDevices.length > 0) ? connectedDevices.join(', ') : 'None'} </Text>
          <Text> {status} </Text>
        </Box>

      </Box>

      <Box
        paddingX={2}
        marginBottom={1}
        justifyContent='space-around'
      >
        <Text>Paste Strings:</Text>
        <Text>[Ctrl-1]trade</Text>
        <Text>[Ctrl-2]trade&!shuppet&!fletchling</Text>
        <Text>[Ctrl-3]!traded&!shadow&!.</Text>
        <Text>[Ctrl-4]traded&!.</Text>
      </Box>

      <Box
        paddingX={2}
        justifyContent='space-around'
      >
        <Text>
          {automaticMode === false
            ? '[S]tep'
            : (true) ? '[S]tart' : '[S]top'
          }
        </Text>
        <Text>[M]ode</Text>
        <Text>[G]et Devices</Text>
        <Text>[↑/↓]Trade Count</Text>
        <Text>[←/→]Step</Text>
      </Box>
    </>
  )
}

const {pasteToAllDevices} = useStore.getState().app

hotkeys.on({
  hotkeys: R.range(0,10).map((number) => `ctrl + ${number}`).join(', '),
  matchAllModifiers: true,
  triggerAll: true,
  callback: (hotkey) => {
    if (useStore.getState().app.activeTab != 'Trade') return
    const number = parseInt(R.last(hotkey))
    if (number == 1) pasteToAllDevices('trade')
    if (number == 2) pasteToAllDevices('trade\\&!shuppet&!fomantis')
    if (number == 3) pasteToAllDevices('!traded\\&!shadow\\&!\\.')
    if (number == 4) pasteToAllDevices('traded\\&!\\.')
  }
})

module.exports = Trade
