#!/usr/bin/env node
'use strict';
const React = require('react')
const importJsx = require('import-jsx')
const {render} = require('ink')
const meow = require('meow')

const Assistant = importJsx('./src/Assistant')

const cli = meow(`
	Usage
	  $ pogo --tab=Gift
`)

process.on('uncaughtException', function(error) {
  console.log('Caught exception: ' + error);
  process.exit()
});

render(React.createElement(Assistant, cli.flags))
