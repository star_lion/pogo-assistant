'use strict';
const React = require('react')
const R = require('ramda')
const {Text, Box, Newline, useInput, useApp} = require('ink')
const nutjs = require('@nut-tree/nut-js');
const create = require('zustand').default
const {produce} = require('immer')
const hotkeys = require('node-hotkeys')

const {mouse, screen, Point, Region, straightTo, sleep} = nutjs

const {isNotNil, relativeFractionPositionToAbsolutePoint, relativeFractionRegionToAbsoluteRegion, absolutePositionToRelativeFractionPosition} = require('nut-js-utils')

mouse.config.mouseSpeed = 10000
screen.config.highlightDurationMs = 750

const useStore = create((set, get) => ({
  activeWindow: {
    title: '',
    x: 0,
    y: 0,
    height: 0,
    width: 0,
  },

  mousePosition: {
    absolute: {
      x: 0,
      y: 0,
    },
    relative: {
      x: 0,
      y: 0,
    },
    relativeFraction: {
      x: 0,
      y: 0,
    }
  },

  mousePointColor: undefined,

  autoClickIntervalId: undefined,

  geometryHistory: {},

  updateHistoryEntry: (number, point, region) => set(produce(state => {
    const isEntryUndefined = R.pathSatisfies(R.isNil, ['geometryHistory', number])(state)
    const isEntryAlreadyRegion = R.pathSatisfies(isNotNil, ['geometryHistory', number, 'region'])(state)

    if (isEntryUndefined || (!isEntryUndefined && isEntryAlreadyRegion)) {
      state.geometryHistory[number] = {
        number,
        cornerPoints: {
          topLeft: point,
          bottomRight: undefined
        },
        relativeFractionPositions: {
          topLeft: absolutePositionToRelativeFractionPosition([point.x, point.y], region),
          bottomRight: undefined
        },
        region: undefined
      }
    } else if (!isEntryUndefined && !isEntryAlreadyRegion) {
      const geometryEntry = state.geometryHistory[number]
      const topLeft = geometryEntry.cornerPoints.topLeft
      geometryEntry.cornerPoints.bottomRight = point
      geometryEntry.relativeFractionPositions.bottomRight = absolutePositionToRelativeFractionPosition([point.x, point.y], region)
      const newRegion = new Region(
        topLeft.x,
        topLeft.y,
        (point.x - topLeft.x),
        (point.y - topLeft.y)
      )
      geometryEntry.region = newRegion

      screen.highlight(newRegion)
      nutjs.clipboard.copy(`[[${geometryEntry.relativeFractionPositions.topLeft}], [${geometryEntry.relativeFractionPositions.bottomRight}]],`)
    }

  })),

  deleteHistory: () => set(produce(state => {
    state.geometryHistory = {}
  })),

  toggleAutoClick: () => set(produce(state => {
    if (state.autoClickIntervalId == undefined) {
      state.autoClickIntervalId = setInterval(() => {
        mouse.leftClick()
      }, 500)
    } else {
      clearInterval(state.autoClickIntervalId)
      state.autoClickIntervalId = undefined
    }
  })),

  highlightRegions: async () => {
    const geometries = Object.values(get().geometryHistory)
    for (const geometry of geometries) {
      if (isNotNil(geometry.region)) await screen.highlight(geometry.region)
    }
  },

}))

const traceActiveWindow = async(window) => {

  const corners = {
    topLeft: new Point(window.x, window.y),
    topRight: new Point((window.x + window.width), window.y),
    bottomLeft: new Point(window.x, (window.y + window.height)),
    bottomRight: new Point((window.x + window.width), (window.y + window.height)),
  }

  await mouse.setPosition(corners.topLeft)
  await mouse.move(straightTo(corners.topLeft))
  await sleep(100)
  await mouse.move(straightTo(corners.topRight))
  await sleep(100)
  await mouse.move(straightTo(corners.bottomRight))
  await sleep(100)
  await mouse.move(straightTo(corners.bottomLeft))
  await sleep(100)
  await mouse.move(straightTo(corners.topLeft))
}



setInterval(async () => {
  const [mousePoint, activeWindow] = await Promise.all([mouse.getPosition(), nutjs.getActiveWindow()])
  const [title, region] = await Promise.all([activeWindow.title, activeWindow.region])
  const {top, left, height, width} = region

  screen.colorAt(mousePoint)
    .then((mousePointColor) => {
      useStore.setState({mousePointColor: mousePointColor.toHex()})
    })
    .catch((error) => {
      useStore.setState({mousePointColor: 'Cursor out of bounds'})
    })

  const relativeX = mousePoint.x - left
  const relativeY = mousePoint.y - top

  const relativeFractionX = (relativeX / width).toFixed(3)
  const relativeFractionY = (relativeY / height).toFixed(3)

  useStore.setState({
    activeWindow: {
      title,
      width,
      height,
      x: left,
      y: top,
    },

    mousePosition: {
      absolute: {
        x: mousePoint.x,
        y: mousePoint.y,
      },
      relative: {
        x: relativeX,
        y: relativeY,
      },
      relativeFraction: {
        x: relativeFractionX,
        y: relativeFractionY,
      }
    },

  })
}, 500)

const ActiveWindow = () => {
  const {title, height, width, x, y} = useStore(state => state.activeWindow)

  return (
    <Box
      marginY={1}
      flexDirection='column'
    >
      <Text underline={true}>
        Active Window
      </Text>
      <Text>Title: {title}</Text>
      <Text>Position: {x}, {y}</Text>
      <Text>Height, Width: {height}, {width}</Text>
    </Box>
  )
}

const Mouse = () => {
  const {absolute, relative, relativeFraction} = useStore(state => state.mousePosition)
  const mousePointColor = useStore(state => state.mousePointColor)

  return (
    <Box
      marginY={1}
      flexDirection='column'
    >
      <Text underline={true}>
        Mouse
      </Text>
      <Text>Absolute Position: {absolute.x}, {absolute.y} </Text>
      <Text>Relative Window Position: {relative.x}, {relative.y} </Text>
      <Text>Relative Window Fraction Position: {relativeFraction.x}, {relativeFraction.y} </Text>
      <Text>Color: {mousePointColor} </Text>
    </Box>
  )
}

const GeometryEntry = ({geometry}) => {
  const {number, region, relativeFractionPositions, cornerPoints: {topLeft, bottomRight}} = geometry
  const rFP = relativeFractionPositions

  return (
    <Box
      marginY={1}
      flexDirection='column'
    >
      <Text underline={true}>Geometry {number}</Text>
      <Text>Top Left: {topLeft != undefined
        ? `${topLeft.x}, ${topLeft.y}`
        : 'undefined'
      }</Text>
      <Text>Bottom Right: {bottomRight != undefined
        ? `${bottomRight.x}, ${bottomRight.y}`
        : 'undefined'
      }</Text>
      <Text>Region: {region != undefined
        ? region.toString()
        : 'undefined'
      }</Text>
      <Text>Relative Top Left: {topLeft != undefined
        ? `${rFP.topLeft[0]}, ${rFP.topLeft[1]}`
        : 'undefined'
      }</Text>
      <Text>Relative Bottom Right: {bottomRight != undefined
        ? `${rFP.bottomRight[0]}, ${rFP.bottomRight[1]}`
        : 'undefined'
      }</Text>

    </Box>
  )
}

const Inspect = () => {
  const geometries = Object.values(useStore(state => state.geometryHistory))

  return (
    <Box
      margin={1}
      borderColor='white'
      borderStyle='single'
      flexDirection='column'
    >
      <Box
        paddingX={2}
        flexDirection='row'
        justifyContent='space-between'
      >
        <Text
          bold={true}
        >
          Inspection Utility
        </Text>
        <Text>[Alt-Q]uit</Text>
      </Box>
      <Box
        paddingX={1}
        borderColor='white'
        borderStyle='single'
        flexDirection='column'
      >
        <ActiveWindow />
        <Mouse />
        {R.map((geometry) => {
          return <GeometryEntry key={geometry.number} geometry={geometry} />
        })(geometries)}
      </Box>
      <Box
        paddingX={2}
        justifyContent='space-around'
      >
        <Text>[Ctrl-Num]Save Point</Text>
        <Text>[Ctrl-H]ighlight Regions</Text>
        <Text>[Ctrl-D]elete Geometries</Text>
        <Text>[Ctrl-T]race Window</Text>
        <Text>[Alt-A]uto Click</Text>
      </Box>
    </Box>
  )
}

const {toggleAutoClick, updateHistoryEntry, deleteHistory, highlightRegions} = useStore.getState()

process.on('exit', () => {
  hotkeys.iohook.unload()
})

;[
  {
    hotkeys: 'alt + q',
    matchAllModifiers: true,
    callback: (hotkey) => {
      process.exit()
    }
  }, {
    hotkeys: 'alt + a',
    matchAllModifiers: true,
    callback: toggleAutoClick
  }, {
    hotkeys: 'ctrl + t',
    matchAllModifiers: true,
    callback: (hotkey) => {
      return
      setTimeout(() => {
        traceActiveWindow(useStore.getState().activeWindow)
      }, 2000)
    }
  }, {
    hotkeys: 'ctrl + d',
    matchAllModifiers: true,
    callback: deleteHistory
  }, {
    hotkeys: 'ctrl + h',
    matchAllModifiers: true,
    callback: highlightRegions
  }
].forEach(registration => hotkeys.on(registration))

hotkeys.on({
  hotkeys: R.range(0,10).map((number) => `ctrl + ${number}`).join(', '),
  matchAllModifiers: true,
  triggerAll: true,
  callback: async (hotkey) => {
    const number = parseInt(R.last(hotkey))
    const [point, activeWindow] = await Promise.all([mouse.getPosition(), nutjs.getActiveWindow()])
    const region = await activeWindow.region
    updateHistoryEntry(number, point, region)
  }
})

module.exports = Inspect
