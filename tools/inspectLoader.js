#!/usr/bin/env node
'use strict';
const React = require('react')
const importJsx = require('import-jsx')
const {render} = require('ink')
const meow = require('meow')

const ui = importJsx('./Inspect.js')

const cli = meow(`
	Usage
	  $ pogo-assistant-inspection-utility
`)

process.on('uncaughtException', function(error) {
  console.log('Caught exception: ' + error);
  process.exit()
});

console.clear()
render(React.createElement(ui, cli.flags))
