const nutjs = require('@nut-tree/nut-js')
require('@nut-tree/template-matcher')
const  AbortController = require("node-abort-controller").default
const {exec} = require('child_process')

const {sleep, screen, Region, imageResource,
  keyboard, Key, mouse, straightTo, randomPointIn, clipboard,
  OptionalSearchParameters,
} = nutjs

;(async () => {
  screen.config.resourceDirectory = './data/images'
  //screen.config.highlightDurationMs = 2000
  screen.config.autoHighlight = true


  // first set exclusion
  //;const searchButton = await screen.find(imageResource('trade-search.png'))
  //;await mouse.setPosition(await randomPointIn(searchButton))
  //;await sleep(100)
  //;await mouse.leftClick()
  //;await sleep(500)
  //;await clipboard.copy('!Level')
  //;await sleep(100)
  // NOTE: keyevent 279 is KEYCODE_PASTE
  //;exec('adb -s 299b2eedfb1c7ece shell input keyevent 279')
  // NOTE: keyevent 66 is ENTER
  //;exec('adb -s 299b2eedfb1c7ece shell "input text !level && input keyevent 61 && input keyevent 66"')
  //;await sleep(2000)
  //;exec('adb -s 299b2eedfb1c7ece shell input keyevent 66')


    // Configure the postion and size of the area you wish Nut to search
    //const searchRegion = new Region(10, 10, 500, 500);
    //const searchRegion = undefined

    // Configure the confidence you wish Nut to have before finding a match
    const confidence = 0.20;

    // Configure whether Nut should try to match across multiple scales of Image
    const searchMultipleScales = true;

    // Configure an Abort controller so that you can cancel the find operation at any time
    const controller = new AbortController();
    const { signal } = controller;

    // Feed your parameters into the OptionalSearchParameters constructor to make sure they fit the spec
    const fullSearchOptionsConfiguration = new OptionalSearchParameters(undefined, confidence, searchMultipleScales, signal);

    // .find() will return the Region where it found a match based on your search parameters and provided Image data
    //const matchRegion = await screen.find(imageResource("image.png"), fullSearchOptionsConfiguration);


  //const gifts = await screen.findAll(imageResource('gift-bundle.png', fullSearchOptionsConfiguration))
  const gifts = await screen.findAll(imageResource('gift-bundle.png'), {confidence: .94})
  console.log(gifts.map((g)=>g.toString()))

    //const cancelFindTimeout = setTimeout(() => {
      //controller.abort();
    //}, 5000);
})()
