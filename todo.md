Todo:
- [ ] app
  - [ ] add logging to common functions
- [ ] gift
  - [ ] sending functionality
- [ ] trade
  - [ ] improve events to be random points within regions, instead of repetitive clicks
  - [ ] randomize slight differences in timings
  - [ ] single mode


Complete:
- [x] app
  - [x] refactor store into slices, reference: https://github.com/pmndrs/zustand/wiki/Splitting-the-store-into-separate-slices
  - [x] log functionality
- [x] inspect tool
  - [x] region creation, highlighting, logging
- [x] gift
  - [x] receiving functionality
- [x] trade
  - [x] test replacing dumb clicking with image scanning, could be more resilient to unforseen errors
  - [x] take cmd argument as initial focused tab
  - [x] automatically turn off screen after uninterrupted set
- [x] refactor activeTab under app
- [x] refactor into components and load by tab focus
- [x] debug window info
  - [x] move auto click to debug
- [x] inspect ui
- [x] relative/absolute points
- [x] refactor store and actions
- [x] implement trade count functionality
